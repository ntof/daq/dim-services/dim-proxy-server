/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-17T09:13:55+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <string>

#include "ProxyServer.h"

#define CFG_PATH "/etc/ntof/proxy-server.xml"

int main(int argc, char **argv)
{
    std::string cfgName = CFG_PATH;
    if (argc > 1)
    {
        cfgName = argv[1];
        if (argc > 2)
        {
            std::string arg2 = argv[2];
            if (arg2 == "--destroy")
            {
                ntof::proxy::ProxyServer p(cfgName, false);
                p.destroyQueue();
                exit(0);
            }
        }
    }
    ntof::proxy::ProxyServer p(cfgName);
    while (1)
    {
        p.run();
    }
    return 0;
}
