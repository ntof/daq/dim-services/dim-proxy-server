/*
 * DataDestination.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: mdonze
 */

#include "DataDestination.h"

#include <iostream>

#include "DataProvider.h"

namespace ntof {
namespace proxy {

DataDestination::DataDestination(const std::string &from,
                                 const std::string &to,
                                 DataProvider *provider) :
    from_(from), to_(to), cmd(to, this), parent(provider)
{}

DataDestination::~DataDestination() {}

void DataDestination::errorReceived(std::string errMsg,
                                    const ntof::dim::DIMXMLCommand * /*cmd*/)
{
    std::cerr << "Received error " << errMsg << std::endl;
}

void DataDestination::dataReceived(pugi::xml_document &newDoc,
                                   const ntof::dim::DIMXMLCommand * /*cmd*/)
{
    pugi::xml_document doc;
    pugi::xml_node aqnNode = doc.append_child("command");
    aqnNode.append_attribute("source").set_value(
        parent->getPublisherName().c_str());
    aqnNode.append_attribute("from").set_value(from_.c_str());
    aqnNode.append_attribute("to").set_value(to_.c_str());
    aqnNode.append_copy(newDoc.first_child());
    parent->sendCommand(doc);
}

} /* namespace proxy */
} /* namespace ntof */
