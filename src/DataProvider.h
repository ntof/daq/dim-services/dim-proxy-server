/*
 * DataPublisher.h
 *
 *  Created on: Feb 6, 2015
 *      Author: mdonze
 */

#ifndef DATAPUBLISHER_H_
#define DATAPUBLISHER_H_

#include <map>
#include <string>

#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include <pugixml.hpp>

#include "DataDestination.h"
#include "DataSource.h"

namespace ntof {
namespace proxy {

class ProxyServer;

/**
 * Represents a data provider
 */
class DataProvider
{
public:
    explicit DataProvider(const std::string &name, ProxyServer *server);
    DataProvider(const DataProvider &) = delete;
    DataProvider &operator=(const DataProvider &) = delete;
    virtual ~DataProvider();
    /**
     * Update data
     * @param doc XML document parsed
     */
    void updateData(pugi::xml_document &doc);

    /**
     * Query hearthbeat
     * @return
     */
    bool heartBeatOK();

    /**
     * Gets the publisher name
     * @return
     */
    std::string &getPublisherName();

    /**
     * Send a command XML document
     * @param doc
     */
    void sendCommand(pugi::xml_document &doc);

private:
    std::string name_;
    std::string cmdQueueName;
    bool heartBeat;
    boost::interprocess::message_queue *mq;

    typedef boost::shared_ptr<DataSource> sourcePtr;
    typedef std::map<std::string, sourcePtr> sourceMap;
    sourceMap sources;

    typedef boost::shared_ptr<DataDestination> destinationPtr;
    typedef std::map<std::string, destinationPtr> destinationMap;
    destinationMap destinations;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DATAPUBLISHER_H_ */
