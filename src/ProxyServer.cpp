/*
 * ProxyServer.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#include "ProxyServer.h"

#include <iostream>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <ConfigMisc.h>
#include <NTOFException.h>
#include <pugixml.hpp>

#include <dis.hxx>

// Maximum XML data, maybe increased
#define MAX_MSG_SIZE 65535
#define MAX_MSG 2000
#define MQ_NAME "proxyMQ"

#define TIMEOUT_SEC 10
#define PROXY_VERSION "1.0.0"

namespace ntof {
namespace proxy {

ProxyServer::ProxyServer(const std::string &cfgPath, bool start) :
    versionStr(PROXY_VERSION),
    maxMsg(MAX_MSG),
    maxMsgSize(MAX_MSG_SIZE),
    queueName(MQ_NAME),
    srvName("ntof-proxy"),
    watchDogThread(&ProxyServer::watchdog, this)
{
    loadConfig(cfgPath);
    if (start)
    {
        DimServer::start(srvName.c_str());
        std::string versionSvc = srvName + "/VERSION";
        versionService = new DimService(versionSvc.c_str(),
                                        const_cast<char *>(versionStr.c_str()));
    }
}

ProxyServer::~ProxyServer() {}

bool ProxyServer::loadConfig(const std::string &cfgPath)
{
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load_file(cfgPath.c_str());
    if (res)
    {
        try
        {
            ntof::utils::ConfigMisc cfg;
            std::string dimDNS = cfg.getDimDns();
            DimServer::setDnsNode(dimDNS.c_str());
        }
        catch (const ntof::NTOFException &e)
        {
            std::cerr
                << "FATAL : Unable to load configuration file! : " << e.what()
                << std::endl;
        }
        pugi::xml_node root = doc.child("proxy");
        pugi::xml_node cfg = root.child("configuration");
        if (cfg)
        {
            pugi::xml_node queueCfg = cfg.child("queue");
            if (queueCfg)
            {
                queueName = queueCfg.attribute("name").as_string(MQ_NAME);
                maxMsg = queueCfg.attribute("count").as_int(MAX_MSG);
                maxMsgSize = queueCfg.attribute("size").as_int(MAX_MSG_SIZE);
            }

            srvName =
                cfg.child("DIMServer").attribute("name").as_string("ntof-proxy");
        }
        else
        {
            std::cerr << "Warning configuration node not found, falling back "
                         "to defaults"
                      << std::endl;
        }
    }
    else
    {
        std::cerr << "FATAL : Unable to parse XML configuration file "
                  << cfgPath << std::endl;
        exit(0);
    }
    return true;
}

/**
 * Run this server
 */
void ProxyServer::run()
{
    boost::interprocess::message_queue mq(boost::interprocess::open_or_create,
                                          queueName.c_str(), maxMsg, maxMsgSize);
    boost::interprocess::message_queue::size_type recvd_size;
    unsigned int priority;
    while (true)
    {
        try
        {
            std::string rcvString;
            rcvString.resize(mq.get_max_msg_size());
            mq.receive(&rcvString[0], mq.get_max_msg_size(), recvd_size,
                       priority);
            rcvString.resize(recvd_size);
            updateService(rcvString);
        }
        catch (boost::interprocess::interprocess_exception &ex)
        {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void ProxyServer::updateService(const std::string &str)
{
    pugi::xml_document doc;
    pugi::xml_parse_result parseResult = doc.load(str.c_str());
    if (parseResult)
    {
        std::string source = doc.first_child().attribute("source").value();
        {
            boost::mutex::scoped_lock lock(svcMutex);
            if (services.find(source) == services.end())
            {
                pubPtr ptr(new DataProvider(source, this));
                services[source] = ptr;
            }
            services[source]->updateData(doc);
        }
    }
    else
    {
        std::cout << "Unable to parse received XML string!" << std::endl;
    }
}

/**
 * Watchdog to monitor heart beats
 */
void ProxyServer::watchdog()
{
    while (1)
    {
        boost::this_thread::sleep(boost::posix_time::seconds(TIMEOUT_SEC));

        {
            boost::mutex::scoped_lock lock(svcMutex);
            for (serviceMap::iterator it = services.begin();
                 it != services.end();)
            {
                if (!it->second->heartBeatOK())
                {
                    std::cout << "No heart-beat on "
                              << it->second->getPublisherName() << std::endl;
                    services.erase(it++);
                }
                else
                {
                    ++it;
                }
            }
        }
    }
}

void ProxyServer::destroyQueue()
{
    std::cout << "IPC message queue " << queueName << " destroyed!"
              << std::endl;
    boost::interprocess::message_queue::remove(queueName.c_str());
}

/**
 * Gets the message queue configuration
 * @param name
 * @param msgMax
 * @param msgMaxSize
 */
void ProxyServer::getQueueConfiguration(std::string &name,
                                        std::size_t &msgMax,
                                        std::size_t &msgMaxSize)
{
    name = queueName;
    msgMax = maxMsg;
    msgMaxSize = maxMsgSize;
}

} /* namespace proxy */
} /* namespace ntof */
