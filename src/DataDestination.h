/*
 * DataDestination.h
 *
 *  Created on: Feb 9, 2015
 *      Author: mdonze
 */

#ifndef DATADESTINATION_H_
#define DATADESTINATION_H_

#include <string>

#include <DIMXMLCommand.h>

namespace ntof {
namespace proxy {
class DataProvider;

class DataDestination : public ntof::dim::DIMXMLCommandHandler
{
public:
    DataDestination(const std::string &from,
                    const std::string &to,
                    DataProvider *provider);
    virtual ~DataDestination();
    virtual void errorReceived(std::string errMsg,
                               const ntof::dim::DIMXMLCommand *cmd) override;
    virtual void dataReceived(pugi::xml_document &doc,
                              const ntof::dim::DIMXMLCommand *cmd) override;

private:
    std::string from_;
    std::string to_;
    ntof::dim::DIMXMLCommand cmd;
    DataProvider *parent;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DATADESTINATION_H_ */
