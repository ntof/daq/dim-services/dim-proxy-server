/*
 * DataSource.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#include "DataSource.h"

#include <iostream>

namespace ntof {
namespace proxy {

DataSource::DataSource(const std::string &name) :
    data(""), svc(name.c_str(), const_cast<char *>(data.c_str()))
{
    std::cout << "Creating new source : " << name << std::endl;
}
DataSource::~DataSource()
{
    std::cout << "Destroying source : " << svc.getName() << std::endl;
}

void DataSource::updateData(const std::string &newData)
{
    data = newData;
    svc.updateService(const_cast<char *>(data.c_str()));
}

} /* namespace proxy */
} /* namespace ntof */
