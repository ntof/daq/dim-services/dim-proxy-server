/*
 * DataSource.h
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#ifndef DATASOURCE_H_
#define DATASOURCE_H_
#include <string>

#include <dis.hxx>

namespace ntof {
namespace proxy {

class DataSource
{
public:
    explicit DataSource(const std::string &name);
    virtual ~DataSource();
    void updateData(const std::string &newData);

private:
    std::string data;
    DimService svc;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DATASOURCE_H_ */
