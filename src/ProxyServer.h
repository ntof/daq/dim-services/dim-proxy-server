/*
 * ProxyServer.h
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#ifndef PROXYSERVER_H_
#define PROXYSERVER_H_
#include <map>
#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include "DataProvider.h"
#include "DataSource.h"

#include <dis.hxx>

namespace ntof {
namespace proxy {

class ProxyServer
{
public:
    explicit ProxyServer(const std::string &cfgPath, bool start = true);
    ProxyServer(const ProxyServer &) = delete;
    ProxyServer &operator=(const ProxyServer &) = delete;
    virtual ~ProxyServer();

    /**
     * Run this server
     */
    void run();

    /**
     * Destroys the IPC message queue
     */
    void destroyQueue();

    /**
     * Gets the message queue configuration
     * @param name
     * @param msgMax
     * @param msgMaxSize
     */
    void getQueueConfiguration(std::string &name,
                               std::size_t &msgMax,
                               std::size_t &msgMaxSize);

private:
    std::string versionStr;
    std::size_t maxMsg;
    std::size_t maxMsgSize;
    std::string queueName;
    std::string srvName;
    boost::thread watchDogThread;
    typedef boost::shared_ptr<DataProvider> pubPtr;
    typedef std::map<std::string, pubPtr> serviceMap;
    serviceMap services;
    DimService *versionService;

    boost::mutex svcMutex;

    void updateService(const std::string &str);
    void watchdog();
    bool loadConfig(const std::string &cfgPath);
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* PROXYSERVER_H_ */
