/*
 * DataPublisher.cpp
 *
 *  Created on: Feb 6, 2015
 *      Author: mdonze
 */

#include "DataProvider.h"

#include <iostream>
#include <sstream>

#include "ProxyServer.h"

#define MAX_MSG_SIZE 65535

namespace ntof {
namespace proxy {

DataProvider::DataProvider(const std::string &name, ProxyServer *server) :
    name_(name), heartBeat(true), mq(NULL)
{
    std::string queueName;
    std::size_t maxMsg;
    std::size_t maxMsgSize;
    server->getQueueConfiguration(queueName, maxMsg, maxMsgSize);
    cmdQueueName = queueName + name + "Cmd";
    mq = new boost::interprocess::message_queue(
        boost::interprocess::open_or_create, cmdQueueName.c_str(), maxMsg,
        maxMsgSize);
    // Nothing special to build
    std::cout << "Creating new data provider : " << name
              << " with command msg queue name : " << cmdQueueName << std::endl;
}

DataProvider::~DataProvider()
{
    // Probably nothing to delete
    std::cout << "Destroying data publisher : " << name_ << std::endl;
    delete mq;
    boost::interprocess::message_queue::remove(cmdQueueName.c_str());
}

/**
 * Update data
 * @param doc XML document parsed
 */
void DataProvider::updateData(pugi::xml_document &doc)
{
    std::string updateType = doc.first_child().name();
    if (updateType == "acquisition")
    {
        // Acquisition update
        pugi::xml_attribute toAttr = doc.first_child().attribute("to");
        if (toAttr)
        {
            std::string toStr = toAttr.value();
            if (sources.find(toStr) == sources.end())
            {
                sourcePtr ptr(new DataSource(toStr));
                sources[toStr] = ptr;
            }
            pugi::xml_document newDoc;
            newDoc.append_copy(doc.first_child().first_child());
            std::ostringstream oss;
            newDoc.save(oss);
            sources[toStr]->updateData(oss.str());
        }
        else
        {
            std::cerr
                << "Missing \"to\" attribute, acquisition update ignored..."
                << std::endl;
        }
    }
    else if (updateType == "heartbeat")
    {
        // Heart beat update
        heartBeat = true;
    }
    else if (updateType == "command")
    {
        // Command update
        pugi::xml_attribute toAttr = doc.first_child().attribute("to");
        pugi::xml_attribute fromAttr = doc.first_child().attribute("from");
        if (toAttr && fromAttr)
        {
            std::string toStr = toAttr.value();
            std::string fromStr = fromAttr.value();
            if (destinations.find(fromStr) == destinations.end())
            {
                destinationPtr ptr(new DataDestination(fromStr, toStr, this));
                destinations[fromStr] = ptr;
            }
        }
        else
        {
            std::cerr << "Missing \"to\" or \"from\" attribute, command update "
                         "ignored..."
                      << std::endl;
        }
    }
    else if (updateType == "remove")
    {
        // Service removal request
        pugi::xml_attribute toAttr = doc.first_child().attribute("to");
        if (toAttr)
        {
            std::string toStr = toAttr.value();
            sourceMap::iterator it = sources.find(toStr);
            if (it != sources.end())
            {
                sources.erase(it);
            }
        }
        else
        {
            std::cerr << "Missing \"to\" attribute, removal update ignored..."
                      << std::endl;
        }
    }
}

/**
 * Query hearthbeat
 * @return
 */
bool DataProvider::heartBeatOK()
{
    bool ret = heartBeat;
    heartBeat = false;
    return ret;
}

/**
 * Gets the publisher name
 * @return
 */
std::string &DataProvider::getPublisherName()
{
    return name_;
}

/**
 * Send a command XML document
 * @param doc
 */
void DataProvider::sendCommand(pugi::xml_document &doc)
{
    std::ostringstream oss;
    doc.save(oss);
    mq->send(oss.str().c_str(), oss.str().size(), 0);
}

} /* namespace proxy */
} /* namespace ntof */
